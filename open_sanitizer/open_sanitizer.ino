

// pinouts assume arduino uno

// HC-SR04 ultrasonic sensor pinout
// VCC -> 5V
// TRIG -> D3
// ECHO -> D2
// GND -> GND
const int trigPin = 3;
const int echoPin = 2;
const int triggerDistance = 10;
const int timeBetweenDispenses = 2000; //ms

bool dispensed = false;

// state machine
static enum { STANDBY, PENDING , DISPENSING, SAVING } state = STANDBY;

int getDistance(){
  int duration, distance;
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  duration = pulseIn(echoPin, HIGH);
  distance = (duration*.0343)/2;
//  Serial.print("Distance: ");
//  Serial.println(distance);
  delay(50);
  return distance;
}

bool detectHand(){
  int err;
  for (int i = 0; i < 6; i+=1){
    if (getDistance() > triggerDistance){
      err++;
    }
  }
  if (err <= 2){
    Serial.print("[detectHand] Hand Detected! error count: ");
    Serial.println(err);
    return true;
  } else {
    Serial.print("[detectHand] no hand. error count: ");
    Serial.println(err);
    return false;
  }
}

void setup() {
  pinMode(trigPin, OUTPUT);
  pinMode(echoPin, INPUT);
  Serial.begin(9600);
  delay(2000);
  Serial.println("Setup Done");
}

void loop() {

  switch (state)
  {
    case STANDBY:
      if (detectHand()){
        state = PENDING;
        Serial.println("[STANDBY] Entering PENDING state");
      }
      break;

    case PENDING:
      // debounce the sensor to ensure there's a hand
      if(!dispensed){
        if(detectHand()){
          state = DISPENSING;
          Serial.println("[PENDING] Entering DISPENSING state");
          break;
        } else {
          state = STANDBY;
          Serial.println("[PENDING] Entering STANDBY state");
          break;
        }
        state = DISPENSING;
        Serial.println("[PENDING] Entering DISPENSING state");
      } else {
        // we've already dispensed once, wait for the hand to be removed
        if (!detectHand()){
          dispensed = false;
          state = STANDBY;
          Serial.println("[PENDING] Entering STANDBY state");
        }
      }

      break;

    case DISPENSING:
      Serial.println("[DISPENSING] Dispensing sanitizer");
      dispensed = true;
      state = PENDING;
      Serial.println("[DISPENSING] Entering PENDING state");
      break;

  }
}
